#/bin/bash
set -e
export TAG=$(date "+%d%m%Y")-$(git rev-parse --short=12 HEAD)

echo 'docker image TAG'
echo $TAG
echo 'docker login -u ${login_name} -p ${login_password}'
docker login -u $login_name -p $login_password

echo 'docker build'
docker build -t ahmadomari/assignment:$TAG . 
echo 'docker push'
docker push ahmadomari/assignment:$TAG
echo 'The ---------------------------------------- END' 
