FROM openjdk:8
EXPOSE 8070
COPY ./artifacts/*.zip /tmp/assignment.zip
RUN  mkdir /u01 ; \
     unzip -d /u01/assignment -o /tmp/assignment.zip 1>/dev/null 2>/dev/null; \
     chmod -R 777 /u01
WORKDIR /u01/assignment
USER nobody
CMD java -jar -Dserver.port=8070 -Dspring.profiles.active=h2 Dspring.datasource.username=$user Dspring.datasource.password=$password assignment*.jar
